package br.com.itau;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        boolean rodando = true;
        Gestao gestao = new Gestao();
        while(rodando){
            System.out.println("Selecione a opção desejada:");
            System.out.println("1 - Cadastrar um terceiro. \n 2 - Cadastrar funcionário.");

            Scanner input = new Scanner(System.in);

            int opcao = input.nextInt();



            if(opcao == 1 ){
                gestao.cadastrarTerceiro();
            }else if(opcao == 2){
                gestao.cadastrarFuncionario();
            }
        }

    }
}
