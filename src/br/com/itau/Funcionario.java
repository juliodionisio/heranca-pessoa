package br.com.itau;

import java.util.Calendar;

public class Funcionario extends Pessoa{


    private int racf;

    public Funcionario(String nome, int cpf, int racf) {
        super(nome, cpf);
        this.racf = racf;
    }

    public int getRacf() {
        return racf;
    }

    public void setRacf(int racf) {
        this.racf = racf;
    }

    @Override
    public void imprimirPessoa(){
        System.out.println("Nome: " + getNome());
        System.out.println("CPF: " + getCpf());
        System.out.println("RACF: " + getRacf());
        /*if (pessoa.getRacf()){
            System.out.println("RACF: " + pessoa.getRacf());
        }*/
    }





}
