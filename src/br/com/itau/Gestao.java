package br.com.itau;

import java.util.LinkedList;
import java.util.Scanner;

public class Gestao {
    Scanner input = new Scanner(System.in);
    private LinkedList<Pessoa> pessoas = new LinkedList<Pessoa>();

    public void cadastrarTerceiro(){
        System.out.println("Qual o nome do terceiro a ser cadastrado?");
        String nome = input.next();
        System.out.println("Qual o cpf do terceiro a ser cadastrado?");
        int cpf = input.nextInt();

        pessoas.add(new Pessoa(nome, cpf));
        listarPessoas();
    }

    public void cadastrarFuncionario(){
        System.out.println("Qual o nome do terceiro a ser cadastrado?");
        String nome = input.next();
        System.out.println("Qual o cpf do terceiro a ser cadastrado?");
        int cpf = input.nextInt();
        System.out.println("Qual o racf do terceiro a ser cadastrado?");
        int racf = input.nextInt();

        pessoas.add(new Funcionario(nome, cpf, racf));
        listarPessoas();
    }

    public void listarPessoas(){
        for(int n = 0; n < pessoas.size(); n++) {
            pessoas.get(n).imprimirPessoa();
            System.out.println("*****");
        }
    }



}
