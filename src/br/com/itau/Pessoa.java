package br.com.itau;

import java.util.Calendar;

public class Pessoa {
    private String nome;
    private int cpf;


    public Pessoa(String nome, int cpf){
        this.nome = nome;
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getCpf() {
        return cpf;
    }

    public void setCpf(int cpf) {
        this.cpf = cpf;
    }

    public void imprimirPessoa(){
        System.out.println("Nome: " + getNome());
        System.out.println("CPF: " + getCpf());
        /*if (pessoa.getRacf()){
            System.out.println("RACF: " + pessoa.getRacf());
        }*/
    }

}
